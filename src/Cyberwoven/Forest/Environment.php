<?php
namespace Cyberwoven\Forest;

class Environment {

    private $database_name;
    private $branch_name;

    public function __construct($database_name = '', $branch_name = '') {
        $this->database_name = $database_name;
        $this->branch_name = $branch_name;
    }

    public function getDatabaseName() {
        return $this->database_name;
    }

    public function getBranchName() {
        return $this->branch_name;
    }

    public static function detect(string $default_db_name = ''): self {

        $db_name = $default_db_name;

        /**
         * This assumes that php.ini (both cli and fpm) has the following in it:
         * 
         *  cw.environment="forest"
         * 
         * Otherwise, we assume the environment is a local sandbox.
         * 
         * What about live? Just hardcode the db name there, don't even use this method.
         */
        if (defined('DRUPAL_ROOT')) {
            if (get_cfg_var('cw.environment') == 'forest') { 
                $branch = basename(dirname(DRUPAL_ROOT));
            } else {
                $raw_branch = exec('git rev-parse --abbrev-ref HEAD');
                $branch = str_replace(['_', '.', '/'], '-', strtolower($raw_branch));
            }

            if ($branch != 'master' && $branch != 'head' && strpos($branch, 'hotfix') === FALSE) {
                $db_name .= '__' . $branch;
            }
        }

        return new self($db_name, $raw_branch ?? $branch);
    }
}
